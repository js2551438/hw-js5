 // 
// Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
// При виклику функція повинна запитати ім'я та прізвище.
// Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
// Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
// Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin(). Вивести у консоль результат виконання функції.
 // 

let firstName = prompt('Яка вас звати?');
let lastName = prompt('Ваше прізвище?');

function createNewUser() {
    const users = {
    firstName,
    lastName,
    getLogin () {
        const newNameUser = firstName[0].toLocaleLowerCase() + lastName.toLocaleLowerCase()
        return newNameUser
    }
}
    return users
}

const newUsers = createNewUser();
console.log(newUsers);

const login = newUsers.getLogin();
console.log(login);